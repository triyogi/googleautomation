package com.yogi.common;

import com.yogi.pageobjectcommands.HistoryPageCommands;
import com.yogi.pageobjectcommands.HomePageCommands;
import com.yogi.pageobjectcommands.TrendingPageCommands;

/**
 * Created by YOGI on 11/09/2017.
 */
public class HomePageUtilitiesFactory {
    public static void selectMenuLink(String linkType) {
        HomePageUtilities linkName = null;

        switch (linkType.toUpperCase()) {
            case "HOME":
                linkName = new HomePageCommands();
                break;
            case "TRANDING":
                linkName = new TrendingPageCommands();
                break;
            case "HISTORY":
                linkName = new HistoryPageCommands();
                break;
            default:
                throw new IllegalArgumentException("Invalid link: " + linkType);
        }
    }

    public static void getPageTitle(String pageTitle) {
        HomePageUtilities title = null;

        switch (pageTitle) {
            case "HOME":
                title = new HomePageCommands();
                break;
            case "TRANDING":
                title = new TrendingPageCommands();
                break;
            case "HISTORY":
                title = new HistoryPageCommands();
                break;
            default:
                throw new IllegalArgumentException("Invalid title: " + pageTitle);
        }
    }
}