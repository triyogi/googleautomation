package com.yogi.common;

import com.yogi.selenium.Driver;
import org.openqa.selenium.By;

/**
 * Created by YOGI on 11/09/2017.
 */
public abstract class HomePageUtilities {

 public boolean youtubeLogo() {
  return Driver.getDriver().findElement(By.id("logo-icon")).isEnabled();
 }

 public boolean getBrowserTitle(String pageTitle){
   return Driver.getDriver().getCurrentUrl().contains(pageTitle.toLowerCase());
 }

 public void selectMenuLink(String linkType) throws InterruptedException {
  Driver.getDriver().findElement(By.linkText(linkType)).click();
 }
}
