package com.yogi.pageobjectcommands;

import com.yogi.common.HomePageUtilities;
import com.yogi.selenium.Driver;
import org.openqa.selenium.By;

/**
 * Created by YOGI on 11/09/2017.
 */
public class HistoryPageCommands extends HomePageUtilities {
    @Override
    public void selectMenuLink(String linkType) throws InterruptedException {
        Thread.sleep(1000);
        Driver.getDriver().findElement(By.linkText(linkType)).click();
    }
}
