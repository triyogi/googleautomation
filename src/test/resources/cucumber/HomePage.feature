Feature: Youtube home page functionalities

  Background:
    Given I am on youtube home page

    @regression
    Scenario: Verify youtube logo on home page
      When I search for youtube logo
      Then I should see it in left up corner of page

      
@wip @smoke @regression
  Scenario Outline: Verify youtube logo on home page
    When I go to the "<menu>" page
    Then I should navigate to "<pagetitle>" page
    Examples:
      | menu  | pagetitle|
      | Home  | YouTube  |
      | Trending |Trending|
      | History | History |