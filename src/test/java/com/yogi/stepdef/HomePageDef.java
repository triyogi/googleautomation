package com.yogi.stepdef;

import com.yogi.pageobjects.HomePage;
import com.yogi.selenium.Driver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

/**
 * Created by YOGI on 11/09/2017.
 */
public class HomePageDef {

    @Given("^I am on youtube home page$")
    public void i_am_on_youtube_home_page() throws Throwable {
        Driver.getDriver();
    }

    @When("^I search for youtube logo$")
    public void i_search_for_youtube_logo() throws Throwable {
        HomePage.GoTO().youtubeLogo();
    }

    @Then("^I should see it in left up corner of page$")
    public void i_should_see_it_in_left_up_corner_of_page() throws Throwable {
        Assert.assertTrue(HomePage.GoTO().youtubeLogo());
    }

    @When("^I go to the \"([^\"]*)\" page$")
    public void i_go_to_the_page(String linkType) throws Throwable {
        HomePage.GoTO().selectMenuLink(linkType);
    }

    @Then("^I should navigate to \"([^\"]*)\" page$")
    public void i_should_navigate_to_page(String pageTitle) throws Throwable {
         Assert.assertTrue(HomePage.GoTO().getBrowserTitle(pageTitle));
    }

}
